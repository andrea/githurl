import React, { Component, Fragment } from 'react'
import PropTypes from 'prop-types'
import { Avatar, Card, Col, List, Row } from 'antd'
import WithLoading from '../WithLoading/WithLoading'

class Profile extends Component {
  state = {
    loading: true,
  }

  componentDidMount() {
    const { user } = this.props
    if (Object.keys(user).length > 0) {
      this.setState(() => ({ loading: false }))
    }
  }

  componentDidUpdate(prevProps) {
    if (this.props !== prevProps) {
      this.setState(() => ({ loading: false }))
    }
  }

  handleUpdateState = userName => {
    const { updateState } = this.props
    this.setState(() => ({ loading: true }))
    updateState(userName)
  }

  render() {
    const { loading } = this.state
    const { user, events, followers, followings } = this.props
    const { Meta } = Card
    const WithLoadingMeta = WithLoading(Meta)

    return (
      <Fragment>
        <Row gutter={16} style={{ marginBottom: 16 }}>
          <Col xs={{ span: 24 }}>
            <Card bordered={false}>
              <WithLoadingMeta
                loading={loading}
                avatar={<Avatar size="large" src={user && user.avatar_url} />}
                title={user && user.name}
                description={user && user.bio}
              />
            </Card>
          </Col>
        </Row>
        <Row gutter={16}>
          <Col
            xs={{ span: 24 }}
            md={{ span: 12 }}
            lg={{ span: 8 }}
            style={{ marginBottom: 16 }}
          >
            <Card title="Recent Activities" bordered={false}>
              <List
                itemLayout="horizontal"
                dataSource={events}
                pagination={{ pageSize: 5, size: 'small' }}
                renderItem={event => (
                  <List.Item>
                    <List.Item.Meta
                      title={event.type}
                      description={event.repo.name}
                    />
                  </List.Item>
                )}
              />
            </Card>
          </Col>
          <Col
            xs={{ span: 24 }}
            md={{ span: 12 }}
            lg={{ span: 8 }}
            style={{ marginBottom: 16 }}
          >
            <Card title="Following" bordered={false}>
              <List
                itemLayout="horizontal"
                dataSource={followings}
                pagination={{ pageSize: 5, size: 'small' }}
                renderItem={following => (
                  <List.Item>
                    <List.Item.Meta
                      avatar={<Avatar src={following.avatar_url} />}
                      title={<a>{following.login}</a>}
                      onClick={() => this.handleUpdateState(following.login)}
                    />
                  </List.Item>
                )}
              />
            </Card>
          </Col>
          <Col
            xs={{ span: 24 }}
            md={{ span: 12 }}
            lg={{ span: 8 }}
            style={{ marginBottom: 16 }}
          >
            <Card title="Followers" bordered={false}>
              <List
                itemLayout="horizontal"
                dataSource={followers}
                pagination={{ pageSize: 5, size: 'small' }}
                renderItem={follower => (
                  <List.Item>
                    <List.Item.Meta
                      avatar={<Avatar src={follower.avatar_url} />}
                      title={<a>{follower.login}</a>}
                      onClick={() => this.handleUpdateState(follower.login)}
                    />
                  </List.Item>
                )}
              />
            </Card>
          </Col>
        </Row>
      </Fragment>
    )
  }
}

Profile.propTypes = {
  user: PropTypes.object,
  events: PropTypes.array,
  followers: PropTypes.array,
  followings: PropTypes.array,
  updateState: PropTypes.func,
}

export default Profile
