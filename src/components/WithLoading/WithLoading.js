import React from 'react'
import PropTypes from 'prop-types'
import { Spin, Icon } from 'antd'

const antIcon = <Icon type="loading" style={{ fontSize: 24 }} spin />

const WithLoading = Component =>
  function WithLoadingComponent({ loading, ...rest }) {
    if (loading) return <Spin indicator={antIcon} />

    return <Component {...rest} />
  }

WithLoading.propTypes = {
  Component: PropTypes.func.isRequired,
}

export default WithLoading
