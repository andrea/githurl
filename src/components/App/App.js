/* eslint-disable camelcase,no-shadow */
import React, { Component } from 'react'
import { Route, Switch } from 'react-router-dom'
import { Layout, notification } from 'antd'

import fetchData from '../../helpers'
import AppHeader from '../AppHeader/AppHeader'
import Sidebar from '../Sidebar/Sidebar'
import Profile from '../Profile/Profile'
import Repos from '../Repos/Repos'
import './App.css'

class App extends Component {
  state = {
    data: {
      user: {},
      repos: [],
      events: [],
      followers: [],
      followings: [],
    },
    collapsed: false,
  }

  updateState = async username => {
    const res = await fetchData(username)

    if (res.err) {
      return this.openNotificationWithIcon('warning', res.err.message)
    }
    return this.setState(() => ({ data: { ...res.data } }))
  }

  handleToggle = () => {
    this.setState(state => ({ collapsed: !state.collapsed }))
  }

  openNotificationWithIcon = (type, description) => {
    // eslint-disable-next-line
    notification[type]({
      duration: 0,
      message: 'Oops!!',
      description: `${description}. Please, try refreshing your browser. If the error persists, try again later.`,
    })
  }

  componentDidMount() {
    // noinspection JSIgnoredPromiseFromCall
    this.updateState('gaearon')
  }

  render() {
    const {
      data: { repos, ...profile },
      collapsed,
    } = this.state
    const { Content, Footer } = Layout

    return (
      <Layout style={{ height: '100vh' }} hasSider>
        <Sidebar collapsed={collapsed} handleToggle={this.handleToggle} />
        <Layout>
          <AppHeader
            handleToggle={this.handleToggle}
            collapsed={collapsed}
            updateState={this.updateState}
          />
          <Content style={{ margin: '24px 24px 0' }}>
            <Switch>
              <Route
                exact
                path="/"
                render={() => (
                  <Profile {...profile} updateState={this.updateState} />
                )}
              />
              <Route
                path="/repos"
                render={() => <Repos repos={repos} collapsed={collapsed} />}
              />
            </Switch>
          </Content>
          <Footer style={{ textAlign: 'center' }}>
            Copyright &copy; {new Date().getFullYear()} GitHurl. Don&apos;t just
            push commits, throw them like you mean it.
          </Footer>
        </Layout>
      </Layout>
    )
  }
}

export default App
