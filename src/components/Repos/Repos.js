import React, { Component } from 'react'
import { Card, Col, Row, Table } from 'antd'
import PropTypes from 'prop-types'
import './Repos.css'

const getColumns = collapsed => [
  {
    width: 250,
    title: 'Name',
    dataIndex: 'name',
    key: 'name',
    // eslint-disable-next-line react/display-name
    render: (text, record) => (
      <a href={record.url} target="_blank" rel="noopener noreferrer">
        {text}
      </a>
    ),
  },
  {
    width: 200,
    title: 'Owner',
    dataIndex: 'owner',
    key: 'owner',
    className: collapsed ? 'hidden' : '',
  },
  {
    title: 'Description',
    dataIndex: 'description',
    key: 'description',
    className: collapsed ? 'hidden' : '',
  },
]

const dataSource = ({ id: key, name, owner, description, html_url: url }) => ({
  key,
  name,
  owner,
  description,
  url,
})

class Repos extends Component {
  state = {
    loading: true,
  }

  componentDidMount() {
    const { repos } = this.props
    if (repos.length > 0) {
      this.setState(() => ({ loading: false }))
    }
  }

  componentDidUpdate(prevProps) {
    if (this.props !== prevProps) {
      this.setState(() => ({ loading: false }))
    }
  }

  render() {
    const { loading } = this.state
    const { repos, collapsed } = this.props

    return (
      <Row gutter={16} style={{ marginBottom: 16 }}>
        <Col xs={{ span: 24 }}>
          <Card title="All repositories" bordered={false}>
            <Table
              bordered
              columns={getColumns(collapsed)}
              dataSource={repos.map(dataSource)}
              loading={loading}
              size="middle"
              style={{ marginTop: 8 }}
            />
          </Card>
        </Col>
      </Row>
    )
  }
}

Repos.propTypes = {
  repos: PropTypes.array.isRequired,
  collapsed: PropTypes.bool.isRequired,
}

export default Repos
