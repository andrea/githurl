import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Col, Icon, Input, Layout, Row } from 'antd'

class AppHeader extends Component {
  state = {
    userName: '',
  }

  handleChangeUserName = e => {
    e.persist()
    this.setState(() => ({ userName: e.target.value }))
  }

  render() {
    const { userName } = this.state
    const { collapsed, handleToggle, updateState } = this.props
    const { Header } = Layout
    const { Search } = Input

    return (
      <Row>
        <Header style={{ background: '#fff', padding: 0, display: 'flex' }}>
          <div className="header-with-searchbar">
            <Col xs={{ span: 4 }} md={{ span: 12 }} lg={{ span: 8 }}>
              <Icon
                className="trigger"
                type={collapsed ? 'menu-unfold' : 'menu-fold'}
                onClick={handleToggle}
              />
            </Col>
            <Col
              xs={{ span: 20 }}
              md={{ span: 12 }}
              lg={{ span: 8, offset: 8 }}
            >
              <Search
                enterButton="Search"
                prefix={
                  <Icon
                    type="user"
                    style={{ color: 'rgba(0,0,0,.25)', paddingLeft: 6 }}
                  />
                }
                value={userName}
                onChange={this.handleChangeUserName}
                placeholder="GitHub username"
                onSearch={value => {
                  if (value.trim()) {
                    // noinspection JSIgnoredPromiseFromCall
                    updateState(value)
                    this.setState(() => ({ userName: '' }))
                  }
                }}
                style={{ paddingLeft: 8 }}
              />
            </Col>
          </div>
        </Header>
      </Row>
    )
  }
}

AppHeader.propTypes = {
  collapsed: PropTypes.bool.isRequired,
  handleToggle: PropTypes.func.isRequired,
  updateState: PropTypes.func.isRequired,
}

export default AppHeader
