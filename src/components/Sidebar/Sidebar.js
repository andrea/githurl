import React from 'react'
import PropTypes from 'prop-types'
import { NavLink, withRouter } from 'react-router-dom'
import { Icon, Layout, Menu } from 'antd'

const Sidebar = ({ collapsed, handleToggle, history }) => {
  const { Sider } = Layout

  return (
    <Sider
      breakpoint="lg"
      collapsedWidth="0"
      collapsed={collapsed}
      collapsible
      onCollapse={handleToggle}
      trigger={null}
    >
      <div className="logo">
        <p>GitHurl</p>
      </div>
      <Menu
        theme="dark"
        mode="inline"
        selectedKeys={[history.location.pathname]}
      >
        <Menu.Item key="/">
          <NavLink to="/" activeClassName="active">
            <Icon type="user" />
            <span className="nav-text">Profile</span>
          </NavLink>
        </Menu.Item>
        <Menu.Item key="/repos">
          <NavLink to="/repos" activeClassName="active">
            <Icon type="book" />
            <span className="nav-text">All Repositories</span>
          </NavLink>
        </Menu.Item>
      </Menu>
    </Sider>
  )
}

Sidebar.propTypes = {
  collapsed: PropTypes.bool.isRequired,
  handleToggle: PropTypes.func.isRequired,
  history: PropTypes.object.isRequired,
}

export default withRouter(Sidebar)
