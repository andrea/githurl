import axios from 'axios'
import { pick } from 'lodash'

const fetchData = async username => {
  try {
    const [
      userResponse,
      repoResponse,
      eventResponse,
      followerResponse,
      followingResponse,
    ] = await Promise.all([
      axios.get(
        `https://api.github.com/users/${username}?client_id=${
          process.env.REACT_APP_GITHUB_CLIENT_ID
        }&client_secret=${process.env.REACT_APP_GITHUB_CLIENT_SECRET}`
      ),
      axios.get(
        `https://api.github.com/users/${username}/repos?client_id=${
          process.env.REACT_APP_GITHUB_CLIENT_ID
        }&client_secret=${process.env.REACT_APP_GITHUB_CLIENT_SECRET}`
      ),
      axios.get(
        `https://api.github.com/users/${username}/events?client_id=${
          process.env.REACT_APP_GITHUB_CLIENT_ID
        }&client_secret=${process.env.REACT_APP_GITHUB_CLIENT_SECRET}`
      ),
      axios.get(
        `https://api.github.com/users/${username}/followers?client_id=${
          process.env.REACT_APP_GITHUB_CLIENT_ID
        }&client_secret=${process.env.REACT_APP_GITHUB_CLIENT_SECRET}`
      ),
      axios.get(
        `https://api.github.com/users/${username}/following?client_id=${
          process.env.REACT_APP_GITHUB_CLIENT_ID
        }&client_secret=${process.env.REACT_APP_GITHUB_CLIENT_SECRET}`
      ),
    ])

    // User
    const user = pick(userResponse.data, [
      'name',
      'bio',
      'avatar_url',
      'followers',
      'following',
    ])

    // Repos
    const repos = repoResponse.data.map(repo =>
      pick(repo, ['id', 'name', 'owner', 'description', 'html_url'])
    )

    // Events
    const events = eventResponse.data.map(event =>
      pick(event, ['type', 'repo'])
    )

    // Followers
    const followers = followerResponse.data.map(follower =>
      pick(follower, ['login', 'html_url', 'avatar_url'])
    )

    // Following
    const followings = followingResponse.data.map(following =>
      pick(following, ['login', 'html_url', 'avatar_url'])
    )

    return { err: null, data: { user, repos, events, followers, followings } }
  } catch (err) {
    return { err }
  }
}

export default fetchData
