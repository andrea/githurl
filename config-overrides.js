/* eslint-disable no-param-reassign,no-unused-vars */
const { injectBabelPlugin } = require('react-app-rewired')

// noinspection JSUnusedLocalSymbols
module.exports = (config, env) => {
  config = injectBabelPlugin(
    ['import', { libraryName: 'antd', libraryDirectory: 'es', style: 'css' }],
    config
  )

  return config
}
