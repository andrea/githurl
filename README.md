# GitHurl

A simple GitHub client built with React and AntDesign UI library.

## Requirements

To run the application you will need to:

- [Install Node.js][install node] runtime environment
- [Create a new OAuth App][create a new oauth app] from your GitHub account.

## Quick Start

To run the application...

- Clone the repository
- Open Terminal, head over the project root folder, then rename the file `.env.example` to `.env`
- Edit you `.env` file and fill up the empty keys related to your GitHub OAuth App.
- From the project's root folder, install the application by running:

```bash
npm install
```

- Start the client application by running:

```bash
npm start
```

Now head to `http://localhost:3000` and see your running app!

[install node]: https://nodejs.org/en/download/
[create a new oauth app]: https://developer.github.com/apps/building-oauth-apps/creating-an-oauth-app/
